# -*- coding: cp1251 -*-
from PySide import QtCore, QtGui
import sys


class ServiceRange(QtGui.QWidget):
    def __init__(self, min=0, max =10, linetext='', title='',flag_line=0):
        QtGui.QWidget.__init__(self)

        self.main_layout = QtGui.QVBoxLayout()
        self.setWindowTitle(title)
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.max = max
        self.min = min
        self.text = linetext
        self.title = title

        self.initUI(flag_line)

    def initUI(self, flag):
        self.setLayout(self.main_layout)
        rblayout = QtGui.QHBoxLayout()
        self.radiobtns = []
        self.radiobtns.append(QtGui.QRadioButton(u'��� �'))
        self.radiobtns.append(QtGui.QRadioButton(u'��� �'))
        self.radiobtns.append(QtGui.QRadioButton(u'��� Z'))
        self.radiobtns[0].setChecked(True)

        rblayout.addWidget(self.radiobtns[0])
        rblayout.addWidget(self.radiobtns[1])
        rblayout.addWidget(self.radiobtns[2])

        self.main_layout.addLayout(rblayout)
        if flag == 0:
            self.line = QtGui.QLineEdit()
            self.main_layout.addWidget(self.line)

            self.validator = QtGui.QIntValidator()
            self.validator.setRange(self.min, self.max)
            self.line.setValidator(self.validator)
        elif flag == 1:
            self.line = QtGui.QDoubleSpinBox()
            self.main_layout.addWidget(self.line)
            self.line.setMaximum(2.0)
            self.line.setMinimum(-2.0)
            self.line.setDecimals(1)
        elif flag == 2:
            self.line = QtGui.QDoubleSpinBox()
            self.label = QtGui.QLabel(u"������� �� -� �� � ������ ���")
            self.main_layout.addWidget(self.label)

            self.main_layout.addWidget(self.line)
            self.line.setMaximum(3.141)
            self.line.setMinimum(-3.141)
            self.line.setDecimals(3)
            buttonLayout = QtGui.QHBoxLayout()
            self.button_start = QtGui.QPushButton(u"�������")
            #self.button_start.clicked.connect(self.do_trajectory)
            buttonLayout.addWidget(self.button_start)
            self.main_layout.addLayout(buttonLayout)
            return

        buttonLayout = QtGui.QHBoxLayout()
        self.button_start = QtGui.QPushButton(u"�����")
        #self.button_start.clicked.connect(self.do_trajectory)
        buttonLayout.addWidget(self.button_start)

        self.button_stop = QtGui.QPushButton(u"����")
        #self.button_stop.clicked.connect(self.do_trajectory)
        buttonLayout.addWidget(self.button_stop)
        self.main_layout.addLayout(buttonLayout)

    def setMax(self, val):
        self.max = val
        self.validator.setRange(self.min, self.max)

    def setMin(self, val):
        self.min = val
        self.validator.setRange(self.min, self.max)

    def setLinetext(self, val):
        self.text = val

    def setTitle(self, val):
        self.setWindowTitle(val)


if __name__=="__main__":
    app = QtGui.QApplication(sys.argv)
    #geom = app.desktop().availableGeometry()
    ex = ServiceRange()
    sys.exit(app.exec_())