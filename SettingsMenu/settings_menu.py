# -*- coding: cp1251 -*-
from PySide import QtCore, QtGui
import sys
from Hardware.hardware import ComSend


class settings_menu(QtGui.QFrame):
    def __init__(self):
        QtGui.QFrame.__init__(self)
        self.setWindowTitle(u'')
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setFrameStyle(1)

        self.main_layout = QtGui.QVBoxLayout()#QGridLayout()

        self.angle = QtGui.QLabel("0.")
        self.angle.setAlignment(QtCore.Qt.AlignCenter)
        self.angle.setStyleSheet('color: blue')
        self.angle.setFixedWidth(40)
        qf = QtGui.QFont()
        qf.setBold(1)
        qf.setPixelSize(16)
        self.angle.setFont(qf)

        temp = QtGui.QHBoxLayout()
        lbl = QtGui.QLabel(u"���� �������� ������� ���������:")
        lbl.setFixedWidth(200)
        temp.addWidget(lbl)

        temp.addWidget(self.angle)

        self.angle_slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.angle_slider.setMinimum(-18)
        self.angle_slider.setMaximum(18)
        self.angle_slider.setTickInterval(1)
        self.angle_slider.setSingleStep(1)
        self.angle_slider.setValue(0)
        temp.addWidget(self.angle_slider)
        self.connect(self.angle_slider, QtCore.SIGNAL('valueChanged(int)'), self.setLabelText)

        self.main_layout.addLayout(temp)#, 1,0)

        temp = QtGui.QHBoxLayout()
        lbl = QtGui.QLabel(u"������� ���������/������� ������:")
        lbl.setFixedWidth(200)
        temp.addWidget(lbl)
        temp.addStretch()
        self.combo_freq = QtGui.QComboBox()
        self.combo_freq.setFixedWidth(100)

        self.combo_freq.addItem("0.125")
        self.combo_freq.addItem("0.25")
        self.combo_freq.addItem("0.5")
        self.combo_freq.addItem("1.0")

        temp.addWidget(self.combo_freq)

        self.main_layout.addLayout(temp)

        temp = QtGui.QHBoxLayout()
        self.button_accept = QtGui.QPushButton(u"���������")
        self.button_accept.setFixedWidth(100)
        self.button_accept.clicked.connect(self.button_accept_pressed)
        temp.addStretch(1)
        temp.addWidget(self.button_accept)
        self.main_layout.addLayout(temp)
        self.setLayout(self.main_layout)

    def setLabelText(self):
        self.angle.setText(str(self.angle_slider.value()*10))

    def button_accept_pressed(self):
        self.emit(QtCore.SIGNAL("setting()"))

    def getAngle(self):
        return float(self.angle.text())

    def getFrequency(self):
        return float(self.combo_freq.currentText())
