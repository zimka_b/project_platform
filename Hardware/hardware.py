from hardware_ext import COMclass
import threading
import time
import numpy as np


def construct(string):
    if string == "-1":
        return None
    tempstring = string.strip("\n")
    try:
        type, timestamp, datarate, resol, data = tempstring.split("!")
    except:
        print(tempstring)

    timestamp = float(timestamp)
    datarate = float(datarate)
    resol = float(resol)

    data = data.split(":")
    for num, item in enumerate(data):
        data[num] = item.split()
    data = np.array(data).astype(float)
    data *= resol
    times = np.array(range(len(data))).astype(float)
    times *= 1./datarate #*(10 ** 3) #(10 ** 3)
    times += timestamp / datarate #* (10 ** 3)#(10 ** 6)

    res = np.zeros((len(times), 7))
    res[:, 0] = times
    res[:, 1:4] = data
    res[:, 4:7] = np.zeros(3)
    if type == "G": #=="G"
        return None
        fp = open("gyro.txt", "a")
        fp.write(str(res) + "\n")
        fp.close()

    else:
        return res.tolist()


def StartComPort(num='1'):
    Com = COMclass()
    Com.set_com_num(num)
    Com.main_cycle()

    temp = Com.check_run()
    while temp == 0:
        temp = Com.check_run()
        time.sleep(0.1)
    if temp == -1:
        return -1
    return Com


def FinishComPort(Com):
    Com.stop()
    return


def ComListen(ComPort):
    try:
        flag = ComPort.reading_flag()
    except:
        return [-1, 0]
    if not flag:
        return [-1, 0]
    checker = ComPort.check_stack()
    if not checker:
        return [0, 0]

    temp = []
    vector = ComPort.get_stack()
    if vector == ["-1"]:
        return [0, 0]

    for x in vector:
        loc = construct(x)
        if loc != None:
            try:
                temp.extend(loc)
            except:
                pass
    return [1, np.array(temp)]


def ComSend(ComPort, strmessage):
    try:
        ComPort.set_message(strmessage)
    except:
        print("ComSendError!")
        return 0
    ComPort.send_message()
    time.sleep(0.1)
    return 1


def tester():
    s = raw_input("Serial number:")
    com = StartComPort(num=s)
    for t in range(2):
        p = ComListen(com)
        print(p[1])
        time.sleep(0.5)
    print('correct')
    raw_input()


if __name__ == "__main__":
    tester()
