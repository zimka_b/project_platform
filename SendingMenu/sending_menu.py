# -*- coding: cp1251 -*-
from PySide import QtCore, QtGui
import sys
from Hardware.hardware import ComSend


class Sending_menu(QtGui.QFrame):
    def __init__(self, com):
        QtGui.QFrame.__init__(self)
        self.setWindowTitle(u'�������� ���������')
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setFrameStyle(1)
        self.com = com

        self.main_layout = QtGui.QVBoxLayout()#QGridLayout()

        pixmap = QtGui.QPixmap("pics/menu.png")

        #self.setFixedWidth(pixmap.size().width()+20)

        lbl = QtGui.QLabel(self)
        lbl.setPixmap(pixmap)
        temp = QtGui.QHBoxLayout()
        temp.addStretch(1)
        temp.addWidget(lbl)
        self.main_layout.addLayout(temp)#, 1,0)
        temp = self.build_parameters_menu()
        self.main_layout.addLayout(temp)#, 2,0)

        self.statusBar = QtGui.QStatusBar()
        self.main_layout.addWidget(self.statusBar)#, 2,0)

        self.setLayout(self.main_layout)#, 1, 0)
        self.initUI()

        self.show()

    def initUI(self):

        self.message = ''

    def setCom(self, com):
        self.com = com

    def build_parameters_menu(self):
        temp = QtGui.QGridLayout()

        temp1 = QtGui.QLabel(u'A, mm, 0-20')
        temp1.setAlignment(QtCore.Qt.AlignCenter)
        temp.addWidget(temp1, 0, 0)
        self.parameter_A = QtGui.QLineEdit()
        self.parameter_A.setValidator(QtGui.QIntValidator())
        temp.addWidget(self.parameter_A, 1,0)

        temp1 = QtGui.QLabel(u't1, ms')
        temp1.setAlignment(QtCore.Qt.AlignCenter)
        temp.addWidget(temp1, 0, 1)
        self.parameter_t1 = QtGui.QLineEdit()
        self.parameter_t1.setValidator(QtGui.QIntValidator())
        temp.addWidget(self.parameter_t1, 1, 1)

        temp1 = QtGui.QLabel(u't2, ms')
        temp1.setAlignment(QtCore.Qt.AlignCenter)
        temp.addWidget(temp1, 0, 2)
        self.parameter_t2 = QtGui.QLineEdit()
        self.parameter_t2.setValidator(QtGui.QIntValidator())
        temp.addWidget(self.parameter_t2, 1, 2)

        temp1 = QtGui.QLabel(u't3, ms')
        temp1.setAlignment(QtCore.Qt.AlignCenter)
        temp.addWidget(temp1, 0, 3)

        self.parameter_t3 = QtGui.QLineEdit()
        self.parameter_t3.setValidator(QtGui.QIntValidator())
        temp.addWidget(self.parameter_t3, 1, 3)

        temp1 = QtGui.QLabel(u'T, ms')
        temp1.setAlignment(QtCore.Qt.AlignCenter)
        temp.addWidget(temp1, 0, 4)
        self.parameter_T = QtGui.QLineEdit()
        self.parameter_T.setValidator(QtGui.QIntValidator())
        temp.addWidget(self.parameter_T, 1, 4)

        self.button_set_message = QtGui.QPushButton(u'���������� ���������')
        self.button_set_message.clicked.connect(self.button_set_message_pressed)
        temp.addWidget(self.button_set_message, 0, 5)

        self.button_send_message = QtGui.QPushButton(u'��������� ���������')
        self.button_send_message.clicked.connect(self.button_send_message_pressed)
        temp.addWidget(self.button_send_message, 1, 5)

        ttemp = QtGui.QHBoxLayout()
        self.start_prefix = QtGui.QCheckBox(u"�����")
        ttemp.addWidget(self.start_prefix)

        self.finish_postfix = QtGui.QCheckBox(u"�����")
        ttemp.addWidget(self.finish_postfix)

        temp.addLayout(ttemp, 2, 5)

        return temp

    def button_set_message_pressed(self):
        if self.com == "":
            self.statusBar.showMessage(u'���������� ������� ��������� ����������')
            return
        try:
            A = float(self.parameter_A.text())
        except:
            self.statusBar.showMessage(u'������ � ��������� �')
            return

        try:
            t1 = float(self.parameter_t1.text())/1000
        except:
            self.statusBar.showMessage(u'������ � ��������� t1')
            return

        try:
            t2 = float(self.parameter_t2.text())/1000
        except:
            self.statusBar.showMessage(u'������ � ��������� t2')
            return

        try:
            t3 = float(self.parameter_t3.text())/1000
        except:
            self.statusBar.showMessage(u'������ � ��������� t3')
            return

        try:
            T = float(self.parameter_T.text())/1000
        except:
            self.statusBar.showMessage(u'������ � ��������� T')
            return

        if T<(t1+t2+t3):
            self.statusBar.showMessage(u'�������� T ������ ���� ������ ����� t1,t2 � t3')
            return

        p1 = int(A*20)
        if p1 < 1: p1 = 1

        p2 = int(t1/(p1*3.1875) * 10**5)
        if p2 < 1: p2 = 1

        p3 = int(t2/3.1875 * 10**5)
        if p3 < 1: p3 = 1

        p4 = int(t3/(p1*3.1875) * 10**5)
        if p4 < 1: p4 = 1

        p5 = int((T-t1-t2-t3)/3.1875 * 10**4)
        if p5 < 1: p5 = 1


        #self.parameter_A.setText(str(float(p1)/20))
        #self.parameter_t1.setText(str(round(p1*p2*3.1875 /10**5, 3)))
        #self.parameter_t2.setText(str(round(p3*3.1875 /10**5, 3)))
        #self.parameter_t3.setText(str(round(p1*p4*3.1875 /10**5, 3)))

        T = p1*p2*3.1875*10**-5 + p3*3.1875*10**-5 + p1*p4*3.1875*10**-5 + p5*3.1875*10**-4
        #self.parameter_T.setText(str(round(T, 3)))

        self.message = '! '+str(p1)+', '+str(p2)+', '+str(p3)+', '+str(p4)+', '+str(p5)+'='
        self.statusBar.showMessage(self.message)

    def button_send_message_pressed(self):
        mess = self.message
        if mess == "":
            self.statusBar.showMessage(u'������ ��������� ������ ���������')
            return

        self.statusBar.showMessage(u'��������� ����������')

        if self.start_prefix.isChecked():
            ComSend(self.com, "#6=")

        ComSend(self.com, mess)

        if self.finish_postfix.isChecked():
            ComSend(self.com, "#0=")

        self.message = ""