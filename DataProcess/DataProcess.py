import numpy as np
from multiprocessing import Process, Value, Array

def np2str(npar):
    s = str(npar).strip("]")
    return s.strip("[")


def datawriter(data_omgw):
    data = np.array(data_omgw).reshape(7,-1)
    name = "data "+str(data[0, 0]) + "-"+str(data[0, -1]) + ".txt"
    f = open(name,"w")
    for point in np.around(data.transpose(), decimals=3):
        f.write(np2str(point))
        f.write("\n")
    f.close()


def processHardData(alldata, lengshift=4000, flagwrite=0):
    delta = len(alldata) - lengshift

    if delta <=0:
        print("error in processHardData")
        return alldata

    if flagwrite:
        curr_data = np.array(alldata).transpose()
        writedata = curr_data[:, 0:lengshift-1]
        omgw = writedata.reshape(-1).tolist()
        a = Array('d', omgw)
        p = Process(target=datawriter, args=(a,))
        p.start()
        #t1 = threading.Thread(target=datawriter, args= [np.array(writedata)])
        #t1.start()
        #datawriter(writedata)

    curr_data = np.zeros(alldata.shape)
    curr_data[0:delta] = alldata[lengshift:lengshift+delta]
    return curr_data

