# -*- coding: cp1251 -*-
import pyqtgraph as pg
from PySide import QtGui, QtCore
import numpy as np
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')


class PlotElem(QtGui.QHBoxLayout):
    def __init__(self, name=''):
        super(PlotElem, self).__init__()
        self.maxtime = 3
        self.initUI(name)
        self.stepdraw = 2


    def initUI(self, name):
        plotlayout = QtGui.QVBoxLayout()
        self.pl = pg.PlotWidget()
        self.pl.setXRange(0,10)
        self.curve_h = self.pl.plot(pen=(0, 100, 255))
        self.curve_t = self.pl.plot(pen=(100, 255, 100))
        self.curve_r = self.pl.plot(pen=(255, 0, 0))

        templ = self.pl.getPlotItem()
        templ.setLabel('bottom', text='Time', units='s')
        templ.setLabel('left', units='g m/(s*s)')
        templ.showGrid(x=1, y=1, alpha=0.3)
        templ.setDownsampling(auto=True)

        templ = self.pl.getViewBox()
        templ.setLimits(xMin=0, minYRange=0.05)
        templ.setMouseEnabled(x=False, y=False)
        plotlayout.addWidget(self.pl)
        self.name = name
        groupBox = QtGui.QGroupBox("Axis " + name)
        radiolayout = QtGui.QVBoxLayout()

        self.radiobtns = []
        self.radiobtns.append(QtGui.QRadioButton(u'����������'))
        self.radiobtns.append(QtGui.QRadioButton(u'���������'))
        self.radiobtns.append(QtGui.QRadioButton(u'������� ��������'))

        radiolayout.addWidget(self.radiobtns[0])
        radiolayout.addWidget(self.radiobtns[1])
        radiolayout.addWidget(self.radiobtns[2])
        radiolayout.addStretch(1)
        groupBox.setLayout(radiolayout)

        self.radiobtns[1].setChecked(True)

        self.addWidget(groupBox)
        self.addLayout(plotlayout)

    def resetRecord(self):
        self.curve_r.clear()
        return

    def setData(self, data, fl='h'):
        data_t = np.copy(data.transpose())
        x = data_t[0]
        counter = 0
        if not(len(x)>1):
            return

        if x[-1] == x[0]:
            counter +=2
        #x -= x[0]
        y = []
        if self.radiobtns[0].isChecked():
            counter += 2

        if self.radiobtns[1].isChecked():
            counter += 1
            if self.name == "X":
                y = data_t[1]
            elif self.name=="Y":
                y = data_t[2]
            elif self.name=="Z":
                y = data_t[3]
            else:
                counter += 1

        if self.radiobtns[2].isChecked():
            counter += 1
            if self.name == "X":
                y = data_t[4]
            elif self.name=="Y":
                y = data_t[5]
            elif self.name=="Z":
                y = data_t[6]
            else:
                counter += 1

        if counter == 1:
            if fl == 'h':
                self.curve_h.setData(x=x, y=y)

            if fl == 't':
                self.curve_t.setData(x=x, y=y)

            if fl == 'r':
                self.curve_r.setData(x=x, y=y)

        return

